#!/usr/bin/env bash

source /build/now.sh
cd /app

##########

now "Composer self-update"
composer self-update 2>&1
