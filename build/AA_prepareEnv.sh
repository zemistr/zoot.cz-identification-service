#!/usr/bin/env bash

source /build/now.sh

now "B_prepareAndRunComposer.sh"
bash /build/B_prepareAndRunComposer.sh

now "Schema"
#php /app/docroot/bin/console doctrine:migrations:migrate -n --allow-no-migration
php /app/docroot/bin/console doctrine:schema:drop --force
php /app/docroot/bin/console doctrine:schema:create
php /app/docroot/bin/console doctrine:fixtures:load -n

chmod -R 0777 /app/var
