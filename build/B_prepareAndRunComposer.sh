#!/usr/bin/env bash

source /build/now.sh
cd /app

##########

now "Composer install"
composer install --no-interaction --optimize-autoloader 2>&1
