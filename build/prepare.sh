#!/usr/bin/env bash

source /build/now.sh

now "A_prepareSystem.sh"
bash /build/A_prepareSystem.sh

now "AA_prepareEnv.sh"
bash /build/AA_prepareEnv.sh
