# README #

## Instalace
Na začátku je potřeba zkopírovat a upravit dle potřeba soubory ve složce `app/docroot/app/config`:
* `parameters.yml.dist` -> `parameters.yml`
* `parameters_test.yml.dist` -> `parameters_test.yml`

Následně je potřeba spustit `docker-compose up` díky čemu se nám vytvoří architektura na které projekt poběží. Soubor `docker-compose.yml` obsahuje i build container, takže se nám sám spustí composer, vytvoří se schéma databáze a vytvoří se fixtures.  

## API dokumentace
Stačí si v prohlížeči otevří adresu `http://172.17.0.1:8000/api/doc`.

## Testy
Na začátku je potřeba přejmenovat soubor `phpunit.xml.dist` na `phpunit.xml`. Soubor je ve složce `/app/docroot`.
Následně stačí jít do složky `/app/docroot` a spustit `php /app/vendor/bin/phpunit /app/docroot`
