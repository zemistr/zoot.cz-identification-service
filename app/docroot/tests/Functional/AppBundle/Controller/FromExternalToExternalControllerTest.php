<?php

namespace Tests\Functional\AppBundle\Controller;

use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Model\Enum\EnumServices;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Tests\Functional\AppBundle\Traits\DoctrineSetUpTrait;
use Tests\Functional\AppBundle\Traits\ResponseAssertsTrait;

/**
 * @package Tests\Functional\AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 *
 * @covers  \AppBundle\Controller\FromExternalToExternalController
 */
class FromExternalToExternalControllerTest extends WebTestCase
{
    use DoctrineSetUpTrait;
    use ResponseAssertsTrait;

    public function testAction()
    {
        $container = $this->getContainer();
        $idGenerator = $container->get('identity.generator.id_generator');

        $identity = Identity::create($idGenerator);
        $fb = ExternalIdentity::create(
          $idGenerator,
          $identity,
          EnumServices::FACEBOOK,
          'fb-id'
        );
        $email = ExternalIdentity::create(
          $idGenerator,
          $identity,
          EnumServices::EMAIL,
          'email-id'
        );
        /////

        $client = static::createClient();
        $client->request(
          'GET',
          '/from/'.$fb->getService().'/'
          .$fb->getExternalId()
          .'/to/'.$email->getService()
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 404);

        $em = $this->em();
        $em->persist($identity);
        $em->persist($fb);
        $em->flush();

        /////

        $client->request(
          'GET',
          '/from/'.$fb->getService().'/'
          .$fb->getExternalId()
          .'/to/'.$email->getService()
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 404);

        /////

        $em->persist($email);
        $em->flush();

        /////

        $client->request(
          'GET',
          '/from/'.$fb->getService().'/'
          .$fb->getExternalId()
          .'/to/'.$email->getService()
        );

        $response = $client->getResponse();
        $id = $this->assertResponse($response, 200);
        $this->assertSame($email->getExternalId(), $id);

        /////

        $client->request(
          'GET',
          '/from/'.$email->getService().'/'
          .$email->getExternalId()
          .'/to/'.$fb->getService()
        );

        $response = $client->getResponse();
        $id = $this->assertResponse($response, 200);
        $this->assertSame($fb->getExternalId(), $id);

        /////

        $client->request(
          'GET',
          '/from/foo/'.$email->getExternalId().'/to/'.$fb->getService()
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 403);

        /////

        $client->request(
          'GET',
          '/from/'.$email->getService().'/'.$email->getExternalId().'/to/foo'
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 403);
    }
}
