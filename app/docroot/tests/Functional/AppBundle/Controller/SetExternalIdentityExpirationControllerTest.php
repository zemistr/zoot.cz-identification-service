<?php

namespace Tests\Functional\AppBundle\Controller;

use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Model\Enum\EnumServices;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Tests\Functional\AppBundle\Traits\DoctrineSetUpTrait;
use Tests\Functional\AppBundle\Traits\ResponseAssertsTrait;

/**
 * @package Tests\Functional\AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 *
 * @covers  \AppBundle\Controller\SetExternalIdentityExpirationController
 */
class SetExternalIdentityExpirationControllerTest extends WebTestCase
{
    use DoctrineSetUpTrait;
    use ResponseAssertsTrait;

    public function testAction()
    {
        $client = static::createClient();

        $container = $this->getContainer();
        $idGenerator = $container->get('identity.generator.id_generator');
        $em = $this->em();

        $identity = Identity::create($idGenerator);
        $fb = ExternalIdentity::create(
          $idGenerator,
          $identity,
          EnumServices::FACEBOOK,
          'fb-id'
        );

        /////

        $expiration = time() + 1;

        $client->request(
          'POST',
          '/to/'.$identity->getId().'/for/'
          .$fb->getService().'/set-expiration/'.$expiration
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 404);

        /////

        $em->persist($identity);
        $em->persist($fb);
        $em->flush();

        /////

        $expiration = time() + 1;

        $client->request(
          'POST',
          '/to/'.$identity->getId().'/for/'
          .$fb->getService().'/set-expiration/'.$expiration
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 200);

        /////

        $client->request('GET', '/'.$identity->getId());

        $response = $client->getResponse();
        $content = $this->assertResponse($response, 200, false);

        $this->assertSame(
          '{"id":"'.$identity->getId()
          .'","external_identities":[{"service":"'.$fb->getService()
          .'","external_id":"'.$fb->getExternalId()
          .'","expired":false}]}',
          $content
        );

        /////

        sleep(2);

        /////

        $client->request('GET', '/'.$identity->getId());

        $response = $client->getResponse();
        $content = $this->assertResponse($response, 200, false);

        $this->assertSame(
          '{"id":"'.$identity->getId()
          .'","external_identities":[{"service":"'.$fb->getService()
          .'","external_id":"'.$fb->getExternalId()
          .'","expired":true}]}',
          $content
        );

        /////

        $client->request(
          'POST',
          '/to/'.$identity->getId().'/for/foo/set-expiration/'.$expiration
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 403);
    }
}
