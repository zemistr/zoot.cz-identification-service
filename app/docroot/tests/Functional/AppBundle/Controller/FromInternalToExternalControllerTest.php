<?php

namespace Tests\Functional\AppBundle\Controller;

use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Model\Enum\EnumServices;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Tests\Functional\AppBundle\Traits\DoctrineSetUpTrait;
use Tests\Functional\AppBundle\Traits\ResponseAssertsTrait;

/**
 * @package Tests\Functional\AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 *
 * @covers  \AppBundle\Controller\FromInternalToExternalController
 */
class FromInternalToExternalControllerTest extends WebTestCase
{
    use DoctrineSetUpTrait;
    use ResponseAssertsTrait;

    public function testAction()
    {
        $client = static::createClient();

        $container = $this->getContainer();
        $idGenerator = $container->get('identity.generator.id_generator');
        $em = $this->em();

        $identity = Identity::create($idGenerator);
        $fb = ExternalIdentity::create(
          $idGenerator,
          $identity,
          EnumServices::FACEBOOK,
          'fb-id'
        );

        /////

        $client->request(
          'GET',
          '/from/'.$identity->getId().'/to/'.$fb->getService()
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 404);

        /////

        $em->persist($identity);
        $em->persist($fb);
        $em->flush();

        /////

        $client->request(
          'GET',
          '/from/'.$identity->getId().'/to/'.$fb->getService()
        );

        $response = $client->getResponse();
        $id = $this->assertResponse($response, 200);
        $this->assertSame($fb->getExternalId(), $id);

        /////

        $client->request('GET', '/from/'.$identity->getId().'/to/foo');

        $response = $client->getResponse();
        $this->assertResponse($response, 403);
    }
}
