<?php

namespace Tests\Functional\AppBundle\Controller;

use IdentityBundle\Entity\Identity;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Tests\Functional\AppBundle\Traits\DoctrineSetUpTrait;
use Tests\Functional\AppBundle\Traits\ResponseAssertsTrait;

/**
 * @package Tests\Functional\AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 *
 * @covers  \AppBundle\Controller\CreateIdentityController
 */
class CreateIdentityControllerTest extends WebTestCase
{
    use DoctrineSetUpTrait;
    use ResponseAssertsTrait;

    public function testAction()
    {
        $client = static::createClient();
        $client->request('POST', '/new-identity');

        $response = $client->getResponse();
        $id = $this->assertResponse($response, 201);
        $identity = $this->em()->find(Identity::class, $id);

        $this->assertInstanceOf(Identity::class, $identity);
    }
}
