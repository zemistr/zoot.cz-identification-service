<?php

namespace Tests\Functional\AppBundle\Controller;

use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Model\Enum\EnumServices;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Tests\Functional\AppBundle\Traits\DoctrineSetUpTrait;
use Tests\Functional\AppBundle\Traits\ResponseAssertsTrait;

/**
 * @package Tests\Functional\AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 *
 * @covers  \AppBundle\Controller\FromExternalToInternalController
 */
class FromExternalToInternalControllerTest extends WebTestCase
{
    use DoctrineSetUpTrait;
    use ResponseAssertsTrait;

    public function testAction()
    {
        $container = $this->getContainer();
        $idGenerator = $container->get('identity.generator.id_generator');

        $identity = Identity::create($idGenerator);
        $fb = ExternalIdentity::create(
          $idGenerator,
          $identity,
          EnumServices::FACEBOOK,
          'fb-id'
        );

        /////

        $client = static::createClient();
        $client->request(
          'GET',
          '/from/'.$fb->getService().'/'.$fb->getExternalId()
        );

        $response = $client->getResponse();
        $this->assertResponse($response, 404);

        $em = $this->em();

        $em->persist($identity);
        $em->persist($fb);
        $em->flush();

        /////

        $client->request(
          'GET',
          '/from/'.$fb->getService().'/'.$fb->getExternalId()
        );

        $response = $client->getResponse();
        $id = $this->assertResponse($response, 200);
        $this->assertSame($identity->getId(), $id);

        /////

        $client->request('GET', '/from/foo/'.$fb->getExternalId());

        $response = $client->getResponse();
        $this->assertResponse($response, 403);
    }
}
