<?php

namespace Tests\Functional\AppBundle\Traits;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait DoctrineTrait
{
    use ContainerAwareTrait;

    protected function em(): EntityManager
    {
        /** @var ContainerInterface $container */
        $container = $this->getContainer();

        return $container->get('doctrine.orm.entity_manager');
    }

    protected function resetSchema(): void
    {
        $em = $this->em();
        $metadata = $em->getMetadataFactory()->getAllMetadata();

        $schemaTool = new SchemaTool($em);
        $schemaTool->dropDatabase();
        $schemaTool->createSchema($metadata);
    }
}
