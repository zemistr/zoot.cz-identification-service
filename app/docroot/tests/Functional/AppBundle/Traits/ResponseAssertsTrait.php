<?php

namespace Tests\Functional\AppBundle\Traits;

use Symfony\Component\HttpFoundation\Response;

trait ResponseAssertsTrait
{
    protected function assertResponse(
      Response $response,
      int $code,
      bool $decode = true
    ) {
        $contentType = $response->headers->get('Content-Type');

        $this->assertEquals($code, $response->getStatusCode());
        $this->assertSame('application/json', $contentType);

        if (!$decode) {
            return $response->getContent();
        }

        return json_decode($response->getContent(), true);
    }
}
