<?php

namespace Tests\Functional\AppBundle\Traits;

trait DoctrineSetUpTrait
{
    use DoctrineTrait;

    protected function setUp(): void
    {
        $this->resetSchema();
    }
}
