<?php

namespace IdentityBundle\Entity\Interfaces;

interface IdInterface
{
    public function getId(): string;
}
