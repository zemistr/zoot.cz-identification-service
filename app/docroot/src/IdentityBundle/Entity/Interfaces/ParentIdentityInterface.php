<?php

namespace IdentityBundle\Entity\Interfaces;

use IdentityBundle\Entity\Identity;

interface ParentIdentityInterface
{
    public function getParent(): Identity;
}
