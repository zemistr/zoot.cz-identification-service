<?php

namespace IdentityBundle\Entity;

use IdentityBundle\Entity\Interfaces\IdInterface;
use IdentityBundle\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use IdentityBundle\Generator\IdGeneratorInterface;

/**
 * Identity
 *
 * @ORM\Table(name="identity")
 * @ORM\Entity(repositoryClass="IdentityBundle\Repository\IdentityRepository")
 */
class Identity implements IdInterface
{
    use IdTrait;

    private function __construct()
    {
    }

    public static function create(IdGeneratorInterface $idGenerator
    ): self {
        $self = new self();
        $self->generateId($idGenerator);

        return $self;
    }
}

