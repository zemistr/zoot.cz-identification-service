<?php

namespace IdentityBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use IdentityBundle\Entity\Identity;

trait ParentIdentityTrait
{
    /**
     * @var Identity
     *
     * @ORM\ManyToOne(targetEntity="IdentityBundle\Entity\Identity", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=false)
     */
    protected $parent;

    public function getParent(): Identity
    {
        return $this->parent;
    }
}
