<?php

namespace IdentityBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use IdentityBundle\Generator\IdGeneratorInterface;

trait IdTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    protected function generateId(IdGeneratorInterface $idGenerator): void
    {
        $this->id = $idGenerator->generate();
    }

    public function getId(): string
    {
        return $this->id;
    }
}
