<?php

namespace IdentityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IdentityBundle\Entity\Interfaces\IdInterface;
use IdentityBundle\Entity\Interfaces\ParentIdentityInterface;
use IdentityBundle\Entity\Traits\IdTrait;
use IdentityBundle\Entity\Traits\ParentIdentityTrait;
use IdentityBundle\Generator\IdGeneratorInterface;
use IdentityBundle\Model\Enum\EnumServices;

/**
 * Identity
 *
 * @ORM\Table(
 *     name="external_identity",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"service", "external_id"})
 *     },
 *     indexes={
 *          @ORM\Index(columns={"service"}),
 *          @ORM\Index(columns={"external_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="IdentityBundle\Repository\ExternalIdentityRepository")
 */
class ExternalIdentity implements IdInterface, ParentIdentityInterface
{
    use IdTrait;
    use ParentIdentityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=20, )
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=255)
     */
    private $externalId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expirationAt", type="datetime", nullable=true)
     */
    private $expirationAt;

    private function __construct()
    {
    }

    public function getService(): string
    {
        return $this->service;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function changeExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function changeExpiration(\DateTime $expirationAt): void
    {
        $this->expirationAt = $expirationAt;
    }

    public function removeExpiration(): void
    {
        $this->expirationAt = null;
    }

    public function isExpired(): bool
    {
        return
          $this->expirationAt !== null
          && $this->expirationAt < new \DateTime();
    }

    public static function create(
      IdGeneratorInterface $idGenerator,
      Identity $parent,
      string $service,
      string $externalId
    ): self {
        EnumServices::isAllowed($service);

        $self = new self();
        $self->generateId($idGenerator);
        $self->service = $service;
        $self->externalId = $externalId;
        $self->parent = $parent;

        return $self;
    }
}

