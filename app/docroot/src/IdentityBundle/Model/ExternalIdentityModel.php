<?php

namespace IdentityBundle\Model;

use IdentityBundle\Entity\ExternalIdentity;
use JMS\Serializer\Annotation as Serializer;

class ExternalIdentityModel
{
    /**
     * @var string
     */
    private $service;

    /**
     * @var string
     */
    private $externalId;

    /**
     * @var bool
     */
    private $expired;

    /**
     * @var string
     * @Serializer\Exclude()
     */
    private $key;

    private function __construct()
    {
    }

    public static function create(ExternalIdentity $externalIdentity): self
    {
        $self = new self();
        $self->service = $externalIdentity->getService();
        $self->externalId = $externalIdentity->getExternalId();
        $self->expired = $externalIdentity->isExpired();
        $self->key = json_encode([$self->getService(), $self->getExternalId()]);

        return $self;
    }

    public function getService(): string
    {
        return $this->service;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function isExpired(): bool
    {
        return $this->expired;
    }

    public function getKey(): string
    {
        return $this->key;
    }
}
