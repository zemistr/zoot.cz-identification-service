<?php

namespace IdentityBundle\Model\Enum;

interface EnumInterface
{
    public static function contains(string $value): bool;

    public static function getAll(): array;
}
