<?php

namespace IdentityBundle\Model\Enum;

use IdentityBundle\Exception\ServiceNotAllowedException;

final class EnumServices implements EnumInterface
{
    const EMAIL = 'email';
    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
    const GOOGLE_PLUS = 'googlePlus';

    public static function contains(string $service): bool
    {
        return isset(self::getAll()[$service]);
    }

    public static function isAllowed(string $service)
    {
        if (!self::contains($service)) {
            throw ServiceNotAllowedException::create($service, self::getAll());
        }
    }

    public static function getAll(): array
    {
        return [
          self::EMAIL => self::EMAIL,
          self::FACEBOOK => self::FACEBOOK,
          self::TWITTER => self::TWITTER,
          self::GOOGLE_PLUS => self::GOOGLE_PLUS,
        ];
    }
}
