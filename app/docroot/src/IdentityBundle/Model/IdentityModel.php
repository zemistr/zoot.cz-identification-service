<?php

namespace IdentityBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use IdentityBundle\Entity\Identity;
use JMS\Serializer\Annotation as Serializer;

class IdentityModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var ArrayCollection
     * @Serializer\Accessor(getter="getExternalIdentities")
     */
    private $externalIdentities;

    private function __construct(Identity $identity)
    {
        $this->id = $identity->getId();
        $this->externalIdentities = new ArrayCollection();
    }

    public static function create(Identity $identity): self
    {
        return new self($identity);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setExternalIdentity(
      ExternalIdentityModel $externalIdentityModel
    ): void {
        $this->externalIdentities->set(
          $externalIdentityModel->getKey(),
          $externalIdentityModel
        );
    }

    /**
     * @return array|ExternalIdentityModel[]
     */
    public function getExternalIdentities(): array
    {
        return array_values($this->externalIdentities->toArray());
    }
}
