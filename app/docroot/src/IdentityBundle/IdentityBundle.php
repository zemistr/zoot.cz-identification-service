<?php

namespace IdentityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @package AppBundle
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class IdentityBundle extends Bundle
{
}
