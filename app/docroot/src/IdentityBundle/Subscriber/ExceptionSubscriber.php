<?php

namespace IdentityBundle\Subscriber;

use IdentityBundle\Exception\NotAllowedException;
use IdentityBundle\Exception\NotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @package IdentityBundle\Subscriber
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
          KernelEvents::EXCEPTION => [
            'onKernelException',
          ],
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $exception = $event->getException();

        if (!$exception) {
            return;
        }

        if (!($exception instanceof NotFoundException || $exception instanceof NotAllowedException)) {
            return;
        }

        $response = JsonResponse::create(
          $exception->getMessage(),
          $exception->getCode()
        );

        $event->setResponse($response);
    }
}
