<?php

namespace IdentityBundle\Service;

use Doctrine\ORM\EntityManager;
use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Exception\ExternalIdentityNotFoundException;
use IdentityBundle\Generator\IdGeneratorInterface;
use IdentityBundle\Model\ExternalIdentityModel;
use IdentityBundle\Model\IdentityModel;
use IdentityBundle\Repository\ExternalIdentityRepository;
use IdentityBundle\Repository\IdentityRepository;

class IdentityService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Doctrine\ORM\EntityRepository|ExternalIdentityRepository
     */
    private $externalRepository;

    /**
     * @var IdGeneratorInterface
     */
    private $idGenerator;

    /**
     * @var \Doctrine\ORM\EntityRepository|IdentityRepository
     */
    private $identityRepository;

    public function __construct(
      EntityManager $entityManager,
      IdGeneratorInterface $idGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->idGenerator = $idGenerator;

        $this->externalRepository = $this->entityManager->getRepository(
          ExternalIdentity::class
        );

        $this->identityRepository = $this->entityManager->getRepository(
          Identity::class
        );
    }

    public function createIdentity(): Identity
    {
        return $this->identityRepository->createIdentity($this->idGenerator);
    }

    public function getExternalIdentity(
      string $service,
      string $id
    ): ExternalIdentity {
        return $this->externalRepository->getByServiceAndExternalId(
          $service,
          $id
        );
    }

    public function getExternalIdentityByIdentityId(
      string $externalService,
      string $identityId
    ): ExternalIdentity {
        /** @var ExternalIdentity $externalIdentity */
        $externalIdentity = $this->externalRepository->getByServiceAndParentId(
          $externalService,
          $identityId
        );

        return $externalIdentity;
    }

    public function getExternalIdentityByIdentity(
      string $service,
      Identity $identity
    ): ExternalIdentity {
        return $this->getExternalIdentityByIdentityId(
          $service,
          $identity->getId()
        );
    }

    public function getIdentity(string $internalId): Identity
    {
        return $this->identityRepository->getById($internalId);
    }

    public function getIdentityWithExternalIdentities(
      string $internalId
    ): IdentityModel {
        $identity = $this->identityRepository->getById($internalId);
        $externalIdentities = $this->externalRepository->findAllByIdentity(
          $identity
        );

        $identityModel = IdentityModel::create($identity);

        foreach ($externalIdentities as $externalIdentity) {
            $identityModel->setExternalIdentity(
              ExternalIdentityModel::create($externalIdentity)
            );
        }

        return $identityModel;
    }

    public function getIdentityByExternalServiceAndId(
      string $service,
      string $id
    ): Identity {
        return $this->getExternalIdentity($service, $id)->getParent();
    }

    public function getExternalIdentityByAnotherExternalIdentity(
      string $fromService,
      string $fromId,
      string $toService
    ): ExternalIdentity {
        $identity = $this->getIdentityByExternalServiceAndId(
          $fromService,
          $fromId
        );

        $externalIdentity = $this->getExternalIdentityByIdentity(
          $toService,
          $identity
        );

        return $externalIdentity;
    }

    public function setExternalIdentityToIdentityId(
      string $internalId,
      string $service,
      string $externalId
    ) {
        $identity = $this->getIdentity($internalId);

        try {
            $externalIdentity = $this->externalRepository->getByServiceAndParentId(
              $service,
              $identity->getId()
            );
        }
        catch (ExternalIdentityNotFoundException $e) {
            $externalIdentity = ExternalIdentity::create(
              $this->idGenerator,
              $identity,
              $service,
              $externalId
            );
        }

        $externalIdentity->changeExternalId($externalId);

        $this->entityManager->persist($externalIdentity);
        $this->entityManager->flush($externalIdentity);

        return $externalIdentity;
    }

    public function setExternalIdentityExpiration(
      string $internalId,
      string $service,
      int $expiration
    ) {
        $identity = $this->getIdentity($internalId);

        $externalIdentity = $this->externalRepository->getByServiceAndParentId(
          $service,
          $identity->getId()
        );

        $externalIdentity->changeExpiration(new \DateTime("@$expiration"));

        $this->entityManager->persist($externalIdentity);
        $this->entityManager->flush($externalIdentity);

        return $externalIdentity;
    }
}
