<?php

namespace IdentityBundle\Generator;

use Ramsey\Uuid\Uuid;

class RamseyUuidIdGenerator implements IdGeneratorInterface
{
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
