<?php

namespace IdentityBundle\Generator;

interface IdGeneratorInterface
{
    public function generate(): string;
}
