<?php

namespace IdentityBundle\Exception;

class NotAllowedException extends Exception
{
    public function __construct($message, $code = 403)
    {
        parent::__construct($message, $code);
    }

    public static function create(string $value): self
    {
        return new self(sprintf('"%s" is not allowed', $value));
    }
}
