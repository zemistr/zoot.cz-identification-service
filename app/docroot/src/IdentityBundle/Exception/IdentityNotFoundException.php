<?php

namespace IdentityBundle\Exception;

class IdentityNotFoundException extends NotFoundException
{
    public static function createForEmail(string $email): self
    {
        return new self(sprintf('Identity for email "%s" not found', $email));
    }

    public static function createForId(string $id): self
    {
        return new self(sprintf('Identity with ID "%s" not found', $id));
    }
}
