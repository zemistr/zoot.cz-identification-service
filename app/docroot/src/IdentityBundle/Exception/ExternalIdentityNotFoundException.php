<?php

namespace IdentityBundle\Exception;

class ExternalIdentityNotFoundException extends NotFoundException
{
    public static function createForId(string $service, string $id): self
    {
        return new self(
          sprintf(
            'External identity for service "%s" with ID "%s" not found',
            $service,
            $id
          )
        );
    }

    public static function createForIdentityId(
      string $service,
      string $identityId
    ): self {
        return new self(
          sprintf(
            'External identity for service "%s" and for Identity with ID "%s" not found',
            $service,
            $identityId
          )
        );
    }
}
