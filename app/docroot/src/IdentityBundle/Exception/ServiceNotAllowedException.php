<?php

namespace IdentityBundle\Exception;

class ServiceNotAllowedException extends NotAllowedException
{
    public static function create(
      string $value,
      array $allowedValues = []
    ): parent {
        $message = 'Service "%s" is not allowed';
        $arguments[] = $value;

        if ($allowedValues) {
            $message .= ' (Allowed services are: %s)';
            $arguments[] = implode(', ', $allowedValues);
        }

        return new self(vsprintf($message, $arguments));
    }
}
