<?php

namespace IdentityBundle\Exception;

class NotFoundException extends Exception
{
    public function __construct($message, $code = 404)
    {
        parent::__construct($message, $code);
    }

    public static function create(string $value): self
    {
        return new self(sprintf('"%s" not found', $value));
    }
}
