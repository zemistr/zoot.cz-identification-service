<?php

namespace IdentityBundle\Exception;

class RepositoryNotFoundException extends NotFoundException
{
    public static function create(string $repository): parent
    {
        return new self(sprintf('Repository "%s" not found', $repository));
    }
}
