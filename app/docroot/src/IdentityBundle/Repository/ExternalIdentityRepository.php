<?php

namespace IdentityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Exception\ExternalIdentityNotFoundException;
use IdentityBundle\Model\Enum\EnumServices;

/**
 * @package IdentityBundle\Repository
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class ExternalIdentityRepository extends EntityRepository
{
    public function getByServiceAndExternalId(
      string $service,
      string $externalId
    ): ExternalIdentity {
        EnumServices::isAllowed($service);

        /** @var ExternalIdentity $entity */
        $entity = $this->findOneBy(
          [
            'service' => $service,
            'externalId' => $externalId,
          ]
        );

        if (!$entity) {
            throw ExternalIdentityNotFoundException::createForId(
              $service,
              $externalId
            );
        }

        return $entity;
    }

    public function getByServiceAndParentId(
      string $service,
      string $parentId
    ): ExternalIdentity {
        EnumServices::isAllowed($service);

        /** @var ExternalIdentity $entity */
        $entity = $this->findOneBy(
          [
            'service' => $service,
            'parent' => $parentId,
          ]
        );

        if (!$entity) {
            throw ExternalIdentityNotFoundException::createForIdentityId(
              $service,
              $parentId
            );
        }

        return $entity;
    }

    /**
     * @param Identity $identity
     * @return array|ExternalIdentity[]
     */
    public function findAllByIdentity(Identity $identity): array
    {
        return $this->findBy(['parent' => $identity]);
    }
}
