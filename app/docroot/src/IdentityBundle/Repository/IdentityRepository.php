<?php

namespace IdentityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Exception\IdentityNotFoundException;
use IdentityBundle\Generator\IdGeneratorInterface;

/**
 * @package IdentityBundle\Repository
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class IdentityRepository extends EntityRepository
{
    public function createIdentity(IdGeneratorInterface $idGenerator): Identity
    {
        $identity = Identity::create($idGenerator);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($identity);
        $entityManager->flush($identity);

        return $identity;
    }

    public function getById(string $id): Identity
    {
        /** @var Identity $entity */
        $entity = $this->find($id);

        if (!$entity) {
            throw IdentityNotFoundException::createForId($id);
        }

        return $entity;
    }
}
