<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class FromExternalToInternalController extends Controller
{
    /**
     * This action will find an internal identity by external identity.
     *
     * @ApiDoc(
     *     description="Find an internal identity by external identity.",
     *     requirements={
     *      {
     *          "name"="fromService",
     *          "dataType"="string",
     *          "description"="Key of the service which will be used as source service."
     *      },
     *      {
     *          "name"="fromId",
     *          "dataType"="string",
     *          "description"="Id used in the source service."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when used service is not allowed",
     *         404="Returned when external identity does not exists"
     *     }
     * )
     *
     * @Route("/from/{fromService}/{fromId}")
     * @Method("GET")
     * @param string $fromService
     * @param string $fromId
     * @return Response
     */
    public function action(
      string $fromService,
      string $fromId
    ): Response {
        $identityService = $this->get('identity.service.identity_service');

        $identity = $identityService->getIdentityByExternalServiceAndId(
          $fromService,
          $fromId
        );

        return $this->json($identity->getId());
    }
}
