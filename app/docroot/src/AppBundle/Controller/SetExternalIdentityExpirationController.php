<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class SetExternalIdentityExpirationController extends Controller
{
    /**
     * This action will set an expiration to the external identity.
     *
     * @ApiDoc(
     *     description="Set an expiration to the external identity.",
     *     requirements={
     *      {
     *          "name"="internalId",
     *          "dataType"="string",
     *          "description"="Internal id."
     *      },
     *      {
     *          "name"="service",
     *          "dataType"="string",
     *          "description"="Key of the service."
     *      },
     *      {
     *          "name"="expiration",
     *          "dataType"="integer",
     *          "description"="Expiration time (timestamp)."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when used service is not allowed",
     *         404={
     *              "Returned when internal identity does not exists",
     *              "Returned when external identity does not exists"
     *         }
     *     }
     * )
     *
     * @Route("/to/{internalId}/for/{service}/set-expiration/{expiration}")
     * @Method("POST")
     * @param string $internalId
     * @param string $service
     * @param int    $expiration
     * @return Response
     */
    public function action(
      string $internalId,
      string $service,
      int $expiration
    ): Response {
        $identityService = $this->get('identity.service.identity_service');

        $identityService->setExternalIdentityExpiration(
          $internalId,
          $service,
          $expiration
        );

        return $this->json([]);
    }
}
