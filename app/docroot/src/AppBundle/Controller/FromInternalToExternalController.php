<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class FromInternalToExternalController extends Controller
{
    /**
     * This action will find an external identity by internal identity.
     *
     * @ApiDoc(
     *     description="Find an internal identity by external identity.",
     *     requirements={
     *      {
     *          "name"="internalId",
     *          "dataType"="string",
     *          "description"="Internal id."
     *      },
     *      {
     *          "name"="toService",
     *          "dataType"="string",
     *          "description"="Key of the service which will be used as target service."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when used service is not allowed",
     *         404={
     *              "Returned when internal identity does not exists",
     *              "Returned when external identity does not exists"
     *         }
     *     }
     * )
     *
     * @Route("/from/{internalId}/to/{toService}", requirements={"internalId"="^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$"})
     * @Method("GET")
     * @param string $internalId
     * @param string $toService
     * @return Response
     */
    public function action(
      string $internalId,
      string $toService
    ): Response {
        $identityService = $this->get('identity.service.identity_service');

        $externalIdentity = $identityService->getExternalIdentityByIdentityId(
          $toService,
          $internalId
        );

        return $this->json($externalIdentity->getExternalId());
    }
}
