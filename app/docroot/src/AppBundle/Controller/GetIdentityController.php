<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class GetIdentityController extends Controller
{
    /**
     * This action will return internal identity with all external identities.
     *
     * @ApiDoc(
     *     description="Get an internal identity with all external identities.",
     *     requirements={
     *      {
     *          "name"="internalId",
     *          "dataType"="string",
     *          "description"="Internal id."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         404="Returned when internal identity does not exists"
     *     }
     * )
     *
     * @Route("/{internalId}", requirements={"internalId"="^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$"})
     * @Method("GET")
     * @param string $internalId
     * @return Response
     */
    public function action(string $internalId): Response
    {
        $identityService = $this->get('identity.service.identity_service');

        $identityModel = $identityService->getIdentityWithExternalIdentities(
          $internalId
        );

        $serializer = $this->get('jms_serializer');
        $data = $serializer->toArray($identityModel);

        return $this->json($data);
    }
}
