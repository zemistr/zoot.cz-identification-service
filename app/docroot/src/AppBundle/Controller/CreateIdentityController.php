<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class CreateIdentityController extends Controller
{
    /**
     * This action will create an internal identity.
     *
     * @ApiDoc(
     *     description="Create an internal identity.",
     *     statusCodes={
     *         201="Returned when successful"
     *     }
     * )
     *
     * @Route("/new-identity")
     * @Method("POST")
     * @return Response
     */
    public function action(): Response
    {
        $identityService = $this->get('identity.service.identity_service');

        return $this->json(
          $identityService->createIdentity()->getId(),
          201
        );
    }
}
