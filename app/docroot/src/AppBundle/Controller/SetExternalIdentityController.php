<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class SetExternalIdentityController extends Controller
{
    /**
     * This action will set an external identity to the internal identity.
     *
     * @ApiDoc(
     *     description="Set an external identity to the internal identity.",
     *     requirements={
     *      {
     *          "name"="internalId",
     *          "dataType"="string",
     *          "description"="Internal id."
     *      },
     *      {
     *          "name"="service",
     *          "dataType"="string",
     *          "description"="Key of the service."
     *      },
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "description"="External id used in the service."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when used service is not allowed",
     *         404="Returned when external identity does not exists"
     *     }
     * )
     *
     * @Route("/to/{internalId}/set/{service}/{id}")
     * @Method("POST")
     * @param string $internalId
     * @param string $service
     * @param string $id
     * @return Response
     */
    public function action(
      string $internalId,
      string $service,
      string $id
    ): Response {
        $identityService = $this->get('identity.service.identity_service');

        $identityService->setExternalIdentityToIdentityId(
          $internalId,
          $service,
          $id
        );

        return $this->json([]);
    }
}
