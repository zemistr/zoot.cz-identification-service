<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package AppBundle\Controller
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class FromExternalToExternalController extends Controller
{
    /**
     * This action will find an external identity by another external identity.
     *
     * @ApiDoc(
     *     description="Find an external identity by another external identity.",
     *     requirements={
     *      {
     *          "name"="fromService",
     *          "dataType"="string",
     *          "description"="Key of the service which will be used as source service."
     *      },
     *      {
     *          "name"="fromId",
     *          "dataType"="string",
     *          "description"="Id used in the source service."
     *      },
     *      {
     *          "name"="toService",
     *          "dataType"="string",
     *          "description"="Key of the service which will be used as target service."
     *      }
     *     },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when used service is not allowed",
     *         404={
     *              "Returned when source external identity does not exists",
     *              "Returned when target external identity does not exists"
     *         }
     *     }
     * )
     *
     * @Route("/from/{fromService}/{fromId}/to/{toService}")
     * @Method("GET")
     * @param string $fromService
     * @param string $fromId
     * @param string $toService
     * @return Response
     */
    public function action(
      string $fromService,
      string $fromId,
      string $toService
    ): Response {
        $identityService = $this->get('identity.service.identity_service');

        $externalIdentity = $identityService->getExternalIdentityByAnotherExternalIdentity(
          $fromService,
          $fromId,
          $toService
        );

        return $this->json($externalIdentity->getExternalId());
    }
}
