<?php
declare(strict_types=1);

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IdentityBundle\Entity\ExternalIdentity;
use IdentityBundle\Entity\Identity;
use IdentityBundle\Model\Enum\EnumServices;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @package AppBundle\DataFixtures\ORM
 * @author  Martin Zeman (Zemistr) <me@zemistr.eu>
 */
class Identities implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder(): int
    {
        return 1;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function load(ObjectManager $manager)
    {
        $idGenerator = $this->container->get('identity.generator.id_generator');

        for ($i = 0; $i < 10; $i++) {
            $identity = Identity::create($idGenerator);
            $manager->persist($identity);

            foreach (EnumServices::getAll() as $service) {
                $externalIdentity = ExternalIdentity::create(
                  $idGenerator,
                  $identity,
                  $service,
                  $idGenerator->generate()
                );
                $manager->persist($externalIdentity);
            }
        }

        $manager->flush();
    }
}
